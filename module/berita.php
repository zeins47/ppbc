<section class="banner-inner">

</section>
<!-- /inner pages banner section -->
<!-- inner headers -->
<section class="inner-header">
<div class="container">
    <!-- Page Heading/Breadcrumbs -->
    <div class="row">
        <div class="col-lg-12">
            <h2 class="page-header">Berita</h2>
            <ol class="breadcrumb">
                <li><a href="index.html">Home</a>
                </li>
                <li class="active">Berita</li>
            </ol>
        </div>
    </div>
    <!-- /.row -->
</div>
</section>
<!-- /inner headers -->
<section class="portfolio-w3ls">
<div class="container">
    <!-- Project One -->
    <div class="row">
        <div class="col-md-7">
            <a href="images/portfolio-img1.jpg" class="lightninBox" data-lb-group="1">
				<div class="hover01 column">
					<div>
						<figure><img class="img-responsive img-hover" src="images/portfolio-img1.jpg" alt="w3layouts" title="w3layouts"></figure>
					</div>
				</div>
            </a>
        </div>
        <div class="col-md-5">
            <h3 class="w3layouts w3 w3l w3ls">Lorem Ipsum Dolor</h3>
            <p class="w3layouts w3 w3l w3ls">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laudantium veniam exercitationem expedita laborum at voluptate. Labore, voluptates totam at aut nemo deserunt rem magni pariatur quos perspiciatis atque eveniet unde.</p>
            <a href="images/portfolio-img1.jpg" class="lightninBox btn btn-primary" data-lb-group="2">View Project <i class="fa fa-angle-right"></i></a>
        </div>
    </div>
    <!-- /.row -->
    <hr>
    <!-- Project Two -->
    <div class="row">
        <div class="col-md-7">
            <a href="images/portfolio-img2.jpg" class="lightninBox" data-lb-group="1">
                <div class="hover01 column">
					<div>
						<figure><img class="img-responsive img-hover" src="images/portfolio-img2.jpg" alt="w3layouts" title="w3layouts"></figure>
					</div>
				</div>
            </a>
        </div>
        <div class="col-md-5">
            <h3 class="agileits agileinfo wthree w3-agileits">consectetur adipiscing</h3>
            <p class="agileits agileinfo wthree w3-agileits">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ut, odit velit cumque vero doloremque repellendus distinctio maiores rem expedita a nam vitae modi quidem similique ducimus! Velit, esse totam tempore.</p>
            <a href="images/portfolio-img2.jpg" class="lightninBox btn btn-primary" data-lb-group="2">View Project <i class="fa fa-angle-right"></i></a>
        </div>
    </div>
    <!-- /.row -->
    <hr>
    <!-- Project Three -->
    <div class="row">
        <div class="col-md-7">
            <a href="images/portfolio-img3.jpg" class="lightninBox" data-lb-group="1">
                <div class="hover01 column">
					<div>
						<figure><img class="img-responsive img-hover" src="images/portfolio-img3.jpg" alt="w3layouts" title="w3layouts"></figure>
					</div>
				</div>
            </a>
        </div>
        <div class="col-md-5">
            <h3 class="agileits-w3layouts agile w3-agile">Quisque sit amet</h3>
            <p class="agileits-w3layouts agile w3-agile">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, temporibus, dolores, at, praesentium ut unde repudiandae voluptatum sit ab debitis suscipit fugiat natus velit excepturi amet commodi deleniti alias possimus!</p>
            <a href="images/portfolio-img3.jpg" class="lightninBox btn btn-primary" data-lb-group="2">View Project <i class="fa fa-angle-right"></i></a>
        </div>
    </div>
    <!-- /.row -->
    <hr>
    <!-- Project Four -->
    <div class="row">

        <div class="col-md-7">
            <a href="images/portfolio-img4.jpg" class="lightninBox" data-lb-group="1">
                <div class="hover01 column">
					<div>
						<figure><img class="img-responsive img-hover" src="images/portfolio-img4.jpg" alt="w3layouts" title="w3layouts"></figure>
					</div>
				</div>
            </a>
        </div>
        <div class="col-md-5">
            <h3>Fusce tempus posuere</h3>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Explicabo, quidem, consectetur, officia rem officiis illum aliquam perspiciatis aspernatur quod modi hic nemo qui soluta aut eius fugit quam in suscipit?</p>
            <a href="images/portfolio-img4.jpg" class="lightninBox btn btn-primary" data-lb-group="2">View Project <i class="fa fa-angle-right"></i></a>
        </div>
    </div>
    <!-- /.row -->
    <hr>
    <!-- Project Five -->
    <div class="row">
        <div class="col-md-7">
            <a href="images/portfolio-img5.jpg" class="lightninBox" data-lb-group="1">
                <div class="hover01 column">
					<div>
						<figure><img class="img-responsive img-hover" src="images/portfolio-img5.jpg" alt="w3layouts" title="w3layouts"></figure>
					</div>
				</div>
            </a>
        </div>
        <div class="col-md-5">
            <h3>finibus arcu eget</h3>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid, quo, minima, inventore voluptatum saepe quos nostrum provident ex quisquam hic odio repellendus atque porro distinctio quae id laboriosam facilis dolorum.</p>
            <a href="images/portfolio-img5.jpg" class="lightninBox btn btn-primary" data-lb-group="2">View Project <i class="fa fa-angle-right"></i></a>
        </div>
    </div>
    <!-- /.row -->
    <hr>
    <!-- Pagination -->
    <div class="row text-center">
        <div class="col-lg-12">
            <ul class="pagination">
                <li>
                    <a href="#">&laquo;</a>
                </li>
                <li class="active">
                    <a href="#">1</a>
                </li>
                <li>
                    <a href="#">2</a>
                </li>
                <li>
                    <a href="#">3</a>
                </li>
                <li>
                    <a href="#">4</a>
                </li>
                <li>
                    <a href="#">5</a>
                </li>
                <li>
                    <a href="#">&raquo;</a>
                </li>
            </ul>
        </div>
    </div>
    <!-- /.row -->
</div>
<!-- /.container -->