<?php include "slider.php" ?>
<section class="welcome">
	<div class="container">	
		<div class="wel-w3ls">
			<h3 class="text-center w3layouts w3 w3l w3ls">Selamat Datang di PPBC</h3>
			<p class="text-center w3layouts w3 w3l w3ls">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent laoreet sed tellus eu placerat. Proin interdum, ex euismod cursus cursus, mi enim sagittis nunc, sed rhoncus nunc quam a odio. Donec ac massa urna. Nullam lacinia in velit nec ornare. Donec luctus sagittis odio a mattis. Etiam tellus diam, fringilla ut facilisis sit amet, volutpat in odio. Sed a eros justo. Pellentesque ultricies, neque nec mattis sagittis, ipsum eros efficitur magna, vitae varius augue lacus vitae libero. Sed eu ante pretium nisi vehicula mattis. Fusce posuere purus sem, eget suscipit risus cursus vitae. Phasellus ac libero placerat turpis lacinia sagittis.
				
			</p>
		</div>
	</div>	
</section>
<!-- /welcome section -->
<!-- info section -->
<section class="info">
	<div class="container">
		<div class="row">
			<div class="col-lg-5 col-md-5 info-w3ls1">
				<a class="wmBox" href="#" data-popup="https://player.vimeo.com/video/33812159?title=0&byline=0&portrait=0">
					<img src="images/ppbc/info-img.jpg" alt="w3layouts" title="w3layouts" class="img-responsive">
					<div class="b-wrapper">
						<i class="fa fa-play-circle-o" aria-hidden="true"></i>
					</div>
				</a>
			</div>
			<div class="col-lg-7 col-md-7 info-w3ls2">
				<div class="info-agile">
					<h2>Kegiatan - Kongres I PPBC 2014</h2>
					<small>(video & photo galery)</small>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque cursus auctor bibendum. Phasellus tincidunt nisi ut justo scelerisque sodales in eu leo. Praesent id ultrices purus. Vestibulum lacinia eros nec nisl luctus blandit. Ut quis tempus lectus. Quisque scelerisque turpis id nunc congue, sed ullamcorper mauris efficitur.</p>
				</div>
			</div>
		</div>	
	</div>
</section>
<!-- info section -->
<!-- team section -->
<section class="team">
	<div class="container">
		<h3 class="text-center agileits agileinfo wthree w3-agileits">Kepengurusan PPBC Pusat</h3>
		<p class="text-center agileits agileinfo wthree w3-agileits">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
		<div class="row">
			<div class="col-lg-3 col-md-3 col-sm-6 team-w3ls">
				<div class="grid">
					<figure class="effect-julia">
						<img src="images/ppbc/team-img1.jpg" alt="w3layouts" class="img-responsive" title="w3layouts"/>
						<figcaption>
							<div>
								<ul class="social-icons2">	
									<li><a href="#"><i class="fa fa-facebook"></i></a></li>
									<li><a href="#"><i class="fa fa-youtube"></i></a></li>
									<li><a href="#"><i class="fa fa-twitter"></i></a></li>
									<li><a href="#"><i class="fa fa-whatsapp"></i></a></li>
									<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
								</ul>	
							</div>
						</figcaption>			
					</figure>
				</div>
				<h4 class="text-center">Elizabeth</h4>
				<p class="team-p1">Founder</p>	
			</div>
			<div class="col-lg-3 col-md-3 col-sm-6 team-w3ls">
				<div class="grid">
					<figure class="effect-julia">
						<img src="images/ppbc/team-img2.jpg" alt="w3layouts" class="img-responsive" title="w3layouts"/>
						<figcaption>
							<div>
								<ul class="social-icons2">	
									<li><a href="#"><i class="fa fa-facebook"></i></a></li>
									<li><a href="#"><i class="fa fa-youtube"></i></a></li>
									<li><a href="#"><i class="fa fa-twitter"></i></a></li>
									<li><a href="#"><i class="fa fa-whatsapp"></i></a></li>
									<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
								</ul>	
							</div>
						</figcaption>			
					</figure>
				</div>
				<h4 class="text-center">Diana</h4>
				<p class="team-p1">President</p>	
			</div>
			<div class="col-lg-3 col-md-3 col-sm-6 team-w3ls">
				<div class="grid">
					<figure class="effect-julia">
						<img src="images/ppbc/team-img3.jpg" alt="w3layouts" class="img-responsive" title="w3layouts"/>
						<figcaption>
							<div>
								<ul class="social-icons2">	
									<li><a href="#"><i class="fa fa-facebook"></i></a></li>
									<li><a href="#"><i class="fa fa-youtube"></i></a></li>
									<li><a href="#"><i class="fa fa-twitter"></i></a></li>
									<li><a href="#"><i class="fa fa-whatsapp"></i></a></li>
									<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
								</ul>	
							</div>
						</figcaption>			
					</figure>
				</div>
				<h4 class="text-center">Max Payne</h4>
				<p class="team-p1">Secretary</p>	
			</div>
			<div class="col-lg-3 col-md-3 col-sm-6 team-w3ls">
				<div class="grid">
					<figure class="effect-julia">
						<img src="images/ppbc/team-img4.jpg" alt="w3layouts" class="img-responsive" title="w3layouts"/>
						<figcaption>
							<div>
								<ul class="social-icons2">	
									<li><a href="#"><i class="fa fa-facebook"></i></a></li>
									<li><a href="#"><i class="fa fa-youtube"></i></a></li>
									<li><a href="#"><i class="fa fa-twitter"></i></a></li>
									<li><a href="#"><i class="fa fa-whatsapp"></i></a></li>
									<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
								</ul>	
							</div>
						</figcaption>			
					</figure>
				</div>
				<h4 class="text-center">Johnny Blaze</h4>
				<p class="team-p1">Manager</p>				
			</div>
		</div>	
	</div>
</section>
<!-- /team section -->
<!-- pricing section -->
<section class="price">
	<div class="container">
		<h3 class="text-center agileits-w3layouts agile w3-agile">Jadilah Bagian Dari Kami</h3>
		<p class="text-center agileits-w3layouts agile w3-agile">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
		<div class="cd-pricing-container cd-has-margins text-center">
			<button class="btn-lg">DAFTAR DISINI</button>
		</div> <!-- .cd-pricing-container -->
	</div>
	<!--pop-up-grid -->
	<div id="popup">
		<div id="small-dialog" class="mfp-hide">
			<div class="pop_up">
				<div class="payment-online-form-left">
					<form action="#" method="post">
						<h4><span class="shipping"> </span>Shipping Details</h4>
						<ul>
							<li><input class="text-box-dark" type="text" name="name" value="First Name" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'First Name';}"></li>
							<li><input class="text-box-dark" type="text" name="name" value="Last Name" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Last Name';}"></li>
						</ul>
						<ul>
							<li><input class="text-box-dark" type="text" name="email" value="Email" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Email';}"></li>
							<li><input class="text-box-dark" type="text" name="phone" value="Phone" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Phone';}"></li>
						</ul>
						<ul>
							<li><input class="text-box-dark" type="text" name="address" value="Address" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Address';}"></li>
							<li><input class="text-box-dark" type="text" name="pincode" value="Pin Code" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Pin Code';}"></li>
						</ul>
						<div class="clear-fix"></div>
						<h4 class="paymenthead"><span class="payment"></span>Payment Details</h4>
						<div class="clear-fix"></div>
						<ul class="payment-type">
							<li><span class="col_checkbox">
								<input id="3" class="css-checkbox1" type="checkbox">
								<label for="3" class="css-label1"></label>
								<a class="visa" href="#"></a>
							</span>												
						</li>
						<li>
							<span class="col_checkbox">
								<input id="4" class="css-checkbox2" type="checkbox">
								<label for="4" class="css-label2"></label>
								<a class="paypal" href="#"></a>
							</span>
						</li>
					</ul>
					<ul>
						<li><input class="text-box-dark" type="text" name="number" value="Card Number" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Card Number';}"></li>
						<li><input class="text-box-dark" type="text" name="name" value="Name on card" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Name on card';}"></li>
					</ul>
					<ul>
						<li><input class="text-box-light hasDatepicker" name="date" type="text" id="datepicker" value="Expiration Date" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Expiration Date';}"><em class="pay-date"> </em></li>
						<li><input class="text-box-dark" type="text" name="number" value="Security Code" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Security Code';}"></li>
					</ul>
					<ul class="payment-sendbtns">
						<li><input type="reset" value="Reset"></li>
						<li><input type="submit" value="Submit"></li>
					</ul>
					<div class="clear-fix"></div>
				</form>
			</div>
		</div>
	</div>
</div>
<!--pop-up-grid-->
</section>
<!-- /pricing section -->
<!-- subscribe section -->
<section class="subs">
	<div class="container">
		<!-- <h3>Subcribe to Our news letter to latest news and offers</h3> -->
		
		<div class="col-lg-6 col-md-6 col-sm-12 text-center">
			<img src="images/ppbc/logos/beacukai-for-footer.png" alt="">
		</div>
		<div class="col-lg-6 col-md-6 col-sm-12 text-center">
			<img src="images/ppbc/logos/ppbc-for-footer.png" alt="">
		</div>
		<div class="clearfix"></div>
		
	</div>
</section>