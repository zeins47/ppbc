<!-- inner pages banner section -->
<section class="banner-inner">
	
</section>
<!-- /inner pages banner section -->
<!-- inner headers -->
<section class="inner-header">
    <div class="container">
        <!-- Page Heading/Breadcrumbs -->
        <div class="row">
            <div class="col-lg-12">
                <h2 class="page-header">Contact Us</h2>
                <ol class="breadcrumb">
                    <li><a href="index.html">Home</a>
                    </li>
                    <li class="active">Contact</li>
                </ol>
            </div>
        </div>
        <!-- /.row -->
	</div>
</section>
<!-- /inner headers -->    
<!-- map -->
<div class="map">
	<iframe class="googlemaps" src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d380510.6741687111!2d-88.01234121699822!3d41.83390417061058!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sin!4v1455598377120" style="border:0" allowfullscreen></iframe>
</div>
<!-- /map -->
<!-- contact section -->
<section class="contact-w3ls">
	<div class="container">
		<h3 class="text-center w3layouts w3 w3l w3ls agileits agileinfo wthree w3-agileits">Write To Us</h3>
		<p class="text-center w3layouts w3 w3l w3ls agileits agileinfo wthree w3-agileits">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>	
		<div class="row">
			<form action="#" method="post" name="sentMessage" id="contactForm" novalidate>
                <div class="col-lg-6 col-md-6 col-sm-6 contact-agile1">    
					<div class="control-group form-group">
                        <div class="controls">
                            <label class="contact-p1">Full Name:</label>
                            <input type="text" class="form-control" name="name" id="name" required data-validation-required-message="Please enter your name.">
                            <p class="help-block"></p>
                        </div>
                    </div>	
                    <div class="control-group form-group">
                        <div class="controls">
                            <label class="contact-p1">Phone Number:</label>
                            <input type="tel" class="form-control" name="phone" id="phone" required data-validation-required-message="Please enter your phone number.">
							<p class="help-block"></p>
						</div>
                    </div>
				</div>
				<div class="col-lg-6 col-md-6 col-sm-6 contact-agile2">
                    <div class="control-group form-group">
                        <div class="controls">
                            <label class="contact-p1">Email Address:</label>
                            <input type="email" class="form-control" name="email" id="email" required data-validation-required-message="Please enter your email address.">
							<p class="help-block"></p>
						</div>
                    </div>
					<div class="control-group form-group">
                        <div class="controls">
                            <label class="contact-p1">Subject:</label>
                            <input type="text" class="form-control" name="subject" id="subject" required data-validation-required-message="Please enter Subject.">
                            <p class="help-block"></p>
                        </div>
                    </div>
				</div>
				<div class="col-lg-12">	
                    <div class="control-group form-group">
                        <div class="controls">
                            <label class="contact-p1">Message:</label>
                            <textarea rows="10" cols="100" class="form-control" name="message" id="message" required data-validation-required-message="Please enter your message" maxlength="999" style="resize:none"></textarea>
							<p class="help-block"></p>
                        </div>
                    </div>
                    <div id="success"></div>
                    <!-- For success/fail messages -->
                    <button type="submit" class="btn btn-primary">Send Message</button>
				</div>	
            </form>            
        </div>	
		<div class="row contact-row">
			<div class="col-md-4 contact-grid1-left">
				<div class="contact-grid1-left1">
					<span class="glyphicon glyphicon-envelope" aria-hidden="true"></span>
					<h4>Contact By Email</h4>
					<p>Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias.</p>
					<ul>
						<li>Mail1: <a href="mailto:info@example.com">info@example1.com</a></li>
						<li>Mail2: <a href="mailto:info@example.com">info@example2.com</a></li>
					</ul>
				</div>
			</div>
			<div class="col-md-4 contact-grid1-left">
				<div class="contact-grid1-left1">
					<span class="glyphicon glyphicon-earphone" aria-hidden="true"></span>
					<h4>Contact By Phone</h4>
					<p>Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias.</p>
					<ul>
						<li>Phone: +0000 123 312</li>
						<li>Fax: +123 312</li>
					</ul>
				</div>
			</div>
			<div class="col-md-4 contact-grid1-left">
				<div class="contact-grid1-left1">
					<span class="glyphicon glyphicon-home" aria-hidden="true"></span>
					<h4>Looking For Address</h4>
					<p>Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias.</p>
					<ul>
						<li>Address: General Pvt 66, Dong Da</li>
						<li>Hanoi, Newyork.</li>
					</ul>
				</div>
			</div>
			<div class="clearfix"> </div>
		</div>
	</div>	
</section>			
<!-- /contact section -->