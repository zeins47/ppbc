<div class="pogoSlider" id="js-main-slider">
	<div class="pogoSlider-slide" data-transition="verticalSlide" data-duration="1000"  style="background-image:url(images/ppbc/banner1.jpg);">
		<div class="pogoSlider-slide-element">
			<h3>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</h3>
			<a href="" class="link1">Read More</a>
		</div>
	</div>
	<div class="pogoSlider-slide " data-transition="blocksReveal" data-duration="1000"  style="background-image:url(images/ppbc/banner2.jpg);">
		<div class="pogoSlider-slide-element">
			<h3>Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</h3>
			<a href="about.html" class="link1">Read More</a>
		</div>
	</div>
	<div class="pogoSlider-slide " data-transition="barRevealDown" data-duration="1000"  style="background-image:url(images/ppbc/banner3.jpg);">
		<div class="pogoSlider-slide-element">
			<h3>When an unknown printer took a galley of type and scrambled it to make a type.</h3>
			<a href="about.html" class="link1">Read More</a>
		</div>
	</div>
	<div class="pogoSlider-slide " data-transition="shrinkReveal" data-duration="1000"  style="background-image:url(images/ppbc/banner4.jpg);">
		<div class="pogoSlider-slide-element">
			<h3>It has survived not only five centuries, but also the leap into electronic.</h3>
			<a href="about.html" class="link1">Read More</a>
		</div>
	</div>
</div>