<?php 
  include "config/koneksi.php";
?>
<section class="top-w3ls">
	<!-- top bar -->	
	<div class="top-bar">
		<div class="container">
			<div class="row">
				<div class="col-lg-8 col-md-8 top-w3ls1">
					<p class="top-p1-lg">PERSATUAN PENSIUNAN BEA & CUKAI.</p>
				</div>
				<div class="col-lg-4 col-md-4 top-w3ls2 text-right">
					<ul class="top-contacts">
						<li class="top-hover"><p><span class="glyphicon glyphicon-envelope"></span> <a href="mailto:support@company.com">support@ppbcpusat.org</a></p>
						<!-- <li class="top-unhover"><p><span class="glyphicon glyphicon-phone-alt"></span> </p> -->
					</ul>			
				</div>
			</div>
		</div>		
	</div>
	<!-- /top bar -->
	<!-- navigation -->
	<div class="navbar-wrapper">
		<div class="container">
			<nav class="navbar navbar-inverse navbar-static-top">
				<div class="container">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<a class="navbar-brand logo-brand" href="index.html">
							<!-- <h1>Terrain</h1> -->
							<img src="images/ppbc/logos/logo-for-header.png" alt="">
						</a>
					</div>
					<div id="navbar" class="navbar-collapse collapse">
						<ul class="nav navbar-nav">
						<?php 
						    $tampil=mysqli_query($con, 	"SELECT * FROM mainmenu where aktif='Y'");
						      $no=1;
							  while ($r=mysqli_fetch_array($tampil)){
							      if ($r["id_main"] == 3){
									echo "<li>
						                <div class='dropdown'>
						                  <button class='dropbtn'>Tentang Kami</button>
						                  <div class='dropdown-content'>";
						            $submenu = mysqli_query($con, "SELECT * FROM submenu where aktif='Y' AND id_main = 3");
							        while ($ra=mysqli_fetch_array($submenu)){
						              echo "<a href='$ra[link_sub]'>$ra[nama_sub]</a>";
						            }        
						            echo" </div>
						                </div>
						            </li>";
						          }else{
							      echo "<li class=''><a href='$r[link]'>$r[nama_menu]</a></li>";
							      }
							      $no++;
							    }
						    ?>
							<!-- <li><a href="?menu=tentangkami&submenu=profile">Tentang Kami</a></li> -->

	<!-- 						<li><a href="service.html">Kegiatan</a></li>
							<li><a href="portfolio.html">Agenda</a></li>
							<li><a href="blog.html">Berita</a></li>
							<li><a href="typo.html">Data Keanggotan</a></li>
							<li><a href="contact.html">Kontak Kami</a></li> -->
						</ul>
					</div>
				</div>
			</nav>	
			<!-- <div class="search-box">
				<div id="sb-search" class="sb-search">
					<form action="#" method="post">
						<input class="sb-search-input" placeholder="Enter your search term..." type="search" name="search" id="search">
						<input class="sb-search-submit" type="submit" value="">
						<span class="sb-icon-search"> </span>
					</form>
				</div>
			</div> -->
		</div>
	</div>
</section>	
