<section class="footer"> 
	<a href="#myPage" title="To Top" class="top">
		<span class="glyphicon glyphicon-chevron-up"></span>
	</a>
	<a href="index.html" class="logo text-center">PPBC PUSAT</a>
	<div class="container">
		<div class="row">
			<div class="col-lg-6 col-md-6 col-sm-6 footer-w3ls1">
				<ul class="footer-links1 cl-effect-4">
					<li><a href="about.html">Tentang Kami</a></li>
					<li><a href="service.html">Kegiatan</a></li>
					<li><a href="portfolio.html">Agenda</a></li>
				</ul>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 footer-w3ls2">
				<ul class="footer-links2 cl-effect-4">
					<li><a href="blog.html">Berita</a></li>
					<li><a href="typo.html">Data Keanggotaan</a></li>
					<li><a href="contact.html">Kontak Kami</a></li>
				</ul>
			</div>
		</div>
		<ul class="social-icons1">
			<li><a href="#"><i class="fa fa-facebook"></i></a></li>
			<li><a href="#"><i class="fa fa-twitter"></i></a></li>
			<li><a href="#"><i class="fa fa-whatsapp"></i></a></li>
			<li><a href="#"><i class="fa fa-youtube"></i></a></li>
		</ul>
		<p class="copyright">&copy; 2016 PPBC Pusat. All rights reserved | Design by <a href="http://w3layouts.com/" target="_blank">W3layouts</a></p>
	</div>
	</section>