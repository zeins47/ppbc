<section class="banner-inner">
</section>
<section class="inner-header">
    <div class="container">
        <!-- Page Heading/Breadcrumbs -->
        <div class="row">
            <div class="col-lg-12">
                <h2 class="page-header">Kegiatan</h2>
                <ol class="breadcrumb">
                    <li><a href="/">Home</a>
                    </li>
                    <li class="active">Kegitan</li>
                </ol>
            </div>
        </div>
        <!-- /.row -->
	</div>
</section>
<!-- /inner headers -->
<section class="blogpost-w3ls">
	<div class="container">
        <!-- Content Row -->
        <div class="row">
            <!-- Blog Post Content Column -->
            <div class="col-lg-8">
				<h3>consectetur adipisicing elit Ducimus vero</h3>
                <!-- Blog Post -->
                <hr>
                <!-- Date/Time -->
                <p class="date-agile w3layouts w3 w3l w3ls agileits agileinfo wthree w3-agileits"><i class="fa fa-clock-o"></i> Posted on November 24, 2016 at 9:00 PM</p>
                <hr>
                <!-- Preview Image -->
				<div class="hover01 column">
					<div>
						<figure><img class="img-responsive" src="images/ppbc/banner2.jpg" alt="w3layouts" title="w3layouts"></figure>
					</div>
				</div>
                <hr>
                <!-- Post Content -->
                <p class="lead w3layouts w3 w3l w3ls agileits-w3layouts agile w3-agile">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                <p class="blogpost-p1 agileits agileinfo wthree w3-agileits agileits-w3layouts agile w3-agile">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ut, tenetur natus doloremque laborum quos iste ipsum rerum obcaecati impedit odit illo dolorum ab tempora nihil dicta earum fugiat. Temporibus, voluptatibus. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eos, doloribus, dolorem iusto blanditiis unde eius illum consequuntur neque dicta incidunt ullam ea hic porro optio ratione repellat perspiciatis. Enim, iure! Lorem ipsum dolor sit amet, consectetur adipisicing elit. Error, nostrum, aliquid, animi, ut quas placeat totam sunt tempora commodi nihil ullam alias modi dicta saepe minima ab quo voluptatem obcaecati? Lorem ipsum dolor sit amet, consectetur adipisicing elit. Harum, dolor quis. Sunt, ut, explicabo, aliquam tenetur ratione tempore quidem voluptates cupiditate voluptas illo saepe quaerat numquam recusandae? Qui, necessitatibus, est!</p>

                <hr>

                <!-- Blog Comments -->
				
                <!-- Comments Form -->
                <div class="comment-w3ls">
					<div class="well">
						<h4>Leave a Comment:</h4>
						<form role="form" action="#" method="post">
							<div class="form-group">
								<textarea class="form-control" rows="3"></textarea>
							</div>
							<button type="submit" class="btn btn-primary">Submit</button>
						</form>
					</div>
				</div>
                <hr>

                <!-- Posted Comments -->
				<div class="comment-agile">
					<!-- Comment -->
					<div class="media">
						<a class="pull-left" href="#">
							<div class="hover01 column">
								<div>
									<figure><img class="media-object" src="images/ppbc/comment-img3.jpg" alt="w3layouts" title="w3layouts"></figure>
								</div>
							</div>
						</a>
						<div class="media-body">
							<h4 class="media-heading">Kate Winslet 
								<small>August 25, 2016 at 9:30 PM</small>
							</h4>
							<p class="blogpost-p2">Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.</p>
						</div>
					</div>

					<!-- Comment -->
					<div class="media">
						<a class="pull-left" href="#">
							<div class="hover01 column">
								<div>
									<figure><img class="media-object" src="images/ppbc/comment-img3.jpg" alt="w3layouts" title="w3layouts"></figure>
								</div>
							</div>
						</a>
						<div class="media-body">
							<h4 class="media-heading">Britney Spears
								<small>August 27, 2016 at 9:30 PM</small>
							</h4>
							<p class="blogpost-p2">Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.</p>
							<!-- Nested Comment -->
							<div class="media">
								<a class="pull-left" href="#">
									<div class="hover01 column">
										<div>
											<figure><img class="media-object" src="images/ppbc/comment-img3.jpg" alt="w3layouts" title="w3layouts"></figure>
										</div>
									</div>
								</a>
								<div class="media-body">
									<h4 class="media-heading">Lady Gaga
										<small>August 30, 2016 at 9:30 PM</small>
									</h4>
									<p class="blogpost-p2">Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.</p>
								</div>
							</div>
							<!-- End Nested Comment -->
						</div>
					</div>
				</div>
            </div>

            <!-- Blog Sidebar Widgets Column -->
            <div class="col-md-4">
				<div class="blogpost-right">
					<!-- Blog Search Well -->
					<div class="well">
						<h4>Search Our Site</h4>
						<form action="#" method="post">
							<div class="input-group">
								<input type="text" class="form-control" name="search" id="search2" placeholder="Search" required/>
								<span class="input-group-btn">
									<button class="btn btn-default" type="submit" ><i class="fa fa-search"></i></button>
								</span>
							</div>
						</form>
						<!-- /.input-group -->
					</div>
					<!-- Blog Categories Well -->
					<div class="well">
						<h4>Blog Categories</h4>
						<div class="row">
							<div class="col-lg-12">
								<ul class="list-unstyled">
									<li><a href="#"><i class="fa fa-star" aria-hidden="true"></i> Lorem ipsum dolor</a></li>
									<li><a href="#"><i class="fa fa-star" aria-hidden="true"></i> consectetur adipiscing</a></li>
									<li><a href="#"><i class="fa fa-star" aria-hidden="true"></i> egestas tincidunt</a></li>
									<li><a href="#"><i class="fa fa-star" aria-hidden="true"></i> turpis vitae feugiat</a></li>
								</ul>
							</div>
						</div>
						<!-- /.row -->
					</div>
					<!-- Side Widget Well -->
					<div class="well">
						<h4>Side Widget Well</h4>
						<p class="blogpost-p3">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Inventore, perspiciatis adipisci accusamus laudantium odit aliquam repellat tempore quos aspernatur vero.</p>
					</div>
				</div>
			</div>
        </div><!-- /.row -->
	</div>
</section>	