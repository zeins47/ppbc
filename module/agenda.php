<!-- inner pages banner section -->
<section class="banner-inner">
	
</section>
<!-- /inner pages banner section -->
<!-- inner headers -->
<section class="inner-header">
    <div class="container">
        <!-- Page Heading/Breadcrumbs -->
        <div class="row">
            <div class="col-lg-12">
                <h2 class="page-header">Agenda</h2>
                <ol class="breadcrumb">
                    <li><a href="index.html">Home</a>
                    </li>
                    <li class="active">Agenda</li>
                </ol>
            </div>
        </div>
        <!-- /.row -->
	</div>
</section>
<!-- /inner headers -->
<section class="blog-w3ls">
	<div class="container">
        <!-- Blog Post Row -->
        <div class="row">
            <div class="col-md-1 text-center">
                <p><i class="fa fa-university fa-4x"></i>
                </p>
                <p class="date-w3ls w3layouts w3 w3l w3ls">November 17, 2016</p>
            </div>
            <div class="col-md-5">
                <a href="blog-post.html">
					<div class="hover01 column">
						<div>
							<figure><img class="img-responsive img-hover" src="images/blog-img1.jpg" alt="w3layouts" title="w3layouts"></figure>
						</div>
					</div>
                </a>
            </div>
            <div class="col-md-6">
                <h3>
                    <a href="blog-post.html">Lorem ipsum dolor</a>
                </h3>
                <p class="blog-agile1 w3layouts w3 w3l w3ls">by <a href="https://w3layouts.com/">W3layouts</a></p>
                <p class="blog-agile2 agileits agileinfo wthree w3-agileits">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                <a class="btn btn-primary" href="blog-post.html">Read More <i class="fa fa-angle-right"></i></a>
            </div>
        </div>
        <!-- /.row -->
        <hr>
        <!-- Blog Post Row -->
        <div class="row">
            <div class="col-md-1 text-center">
                <p><i class="fa fa-building-o fa-4x"></i>
                </p>
                <p class="date-w3ls agileits agileinfo wthree w3-agileits">November 20, 2016</p>
            </div>
            <div class="col-md-5">
                <a href="blog-post.html">
                    <div class="hover01 column">
						<div>
							<figure><img class="img-responsive img-hover" src="images/blog-img2.jpg" alt="w3layouts" title="w3layouts"></figure>
						</div>
					</div>
                </a>
            </div>
            <div class="col-md-6">
                <h3><a href="blog-post.html">consectetur adipiscing</a>
                </h3>
                <p class="blog-agile1 agileits-w3layouts agile w3-agile">by <a href="https://w3layouts.com/">W3layouts</a>
                </p>
                <p class="blog-agile2 agileits-w3layouts agile w3-agile">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                <a class="btn btn-primary" href="blog-post.html">Read More <i class="fa fa-angle-right"></i></a>
            </div>
        </div>
        <!-- /.row -->
        <hr>
        <!-- Blog Post Row -->
        <div class="row">
            <div class="col-md-1 text-center">
                <p><i class="fa fa-money fa-4x"></i>
                </p>
                <p class="date-w3ls">November 22, 2016</p>
            </div>
            <div class="col-md-5">
                <a href="blog-post.html">
                    <div class="hover01 column">
						<div>
							<figure><img class="img-responsive img-hover" src="images/blog-img3.jpg" alt="w3layouts" title="w3layouts"></figure>
						</div>
					</div>
                </a>
            </div>
            <div class="col-md-6">
                <h3><a href="blog-post.html">Mauris egestas tincidunt</a>
                </h3>
                <p class="blog-agile1">by <a href="https://w3layouts.com/">W3layouts</a>
                </p>
                <p class="blog-agile2">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                <a class="btn btn-primary" href="blog-post.html">Read More <i class="fa fa-angle-right"></i></a>
            </div>
        </div>
        <!-- /.row -->
	</div>
</section>
	<div class="container">	
        <hr>
        <!-- Pager -->
        <div class="row">
            <ul class="pager">
                <li class="previous"><a href="#">&larr; Older</a>
                </li>
                <li class="next"><a href="#">Newer &rarr;</a>
                </li>
            </ul>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container -->	
<!-- footer section -->