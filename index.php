<?
include "config/koneksi.php";
$go=$_GET[module];
?>
<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html>
<head>
<title>PPBC</title>
<!--mobile apps-->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Terrain Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
	Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- fonts -->
<link href='//fonts.googleapis.com/css?family=Ubuntu+Condensed' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Titillium+Web:400,200,300,600,700,900' rel='stylesheet' type='text/css'>
<!--link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.2/css/ppbc/font-awesome.min.css" media="all" rel="stylesheet" type="text/css"-->
<!-- /fonts -->
<!-- css files -->
<link href="css/ppbc/bootstrap.min.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/ppbc/font-awesome.min.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/ppbc/pricetable.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/ppbc/main.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/ppbc/pogo-slider.min.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/ppbc/style.css" rel="stylesheet" type="text/css" media="all" />
<!-- /css files -->
<!-- js files -->
<script src="js/ppbc/modernizr.js"></script>
<!-- /js files -->
</head>
<body id="myPage" data-spy="scroll" data-target=".navbar" data-offset="60">
  <?php include "menu.php" ?>
  <?php
	  // switch ($go){
		 //  default : $jdl="";$link="home.php";break;
		 //  case "kegiatan": $jdl="";$jdl2="&nbsp;&nbsp;&raquo;&nbsp;";$link="/module/kegiatan.php";break;
		 //  case "profile": $jdl="";$jdl2="&nbsp;&nbsp;&raquo;&nbsp;";$link="profile.php";break;
		 //  case "nuptk": $jdl="";$jdl2="&nbsp;&nbsp;&raquo;&nbsp;";$link="nuptk.php";break;
		 //  case "bukutamu": $jdl="";$jdl2="&nbsp;&nbsp;&raquo;&nbsp;";$link="bukutamu.php";break;
		 //  case "ppdbonline": $jdl="";$jdl2="&nbsp;&nbsp;&raquo;&nbsp;";$link="ppdbonline.php";break;
		 //  case "berita": $jdl="";$jdl2="&nbsp;&nbsp;&raquo;&nbsp;";$link="berita.php";break;
	  // }		
	  if ($_GET[module] == ""){
        include "home.php";
	  }else if ($_GET[module] == "kegiatan"){
        include "module/kegiatan.php";
	  }else if ($_GET[module] == "berita"){
        include "module/berita.php";
	  }else if ($_GET[module] == "agenda"){
        include "module/agenda.php";
	  }else if ($_GET[module] == "member"){
        include "module/anggota.php";
	  }else if ($_GET[module] == "contact"){
        include "module/kontak.php";
	  }else if ($_GET[module] == "visidanmisi"){
        include "module/visimisi.php";
	  }else if ($_GET[module] == "strukturorganisasi"){
        include "module/struktur.php";
	  }else if ($_GET[module] == "profile"){
        include "module/profile.php";
	  }else if ($_GET[module] == "fungsi"){
        include "module/tugas.php";
	  }else if ($_GET[module] == "logo"){
        include "module/logo.php";
	  }else if ($_GET[module] == "mars"){
        include "module/mars.php";
	  }
	?> 
  <?php include "footer.php" ?>
  <a href="#0" class="cd-top">Top</a>

	<script src="js/ppbc/jquery.min.js"></script>
	<script src="js/ppbc/bootstrap.min.js"></script>
	<script src="js/ppbc/SmoothScroll.min.js"></script>

	<!-- scroll to top -->
	<script>
	$(document).ready(function(){
	  // Add smooth scrolling to all links in navbar + footer link
	  $("section.footer a[href='#myPage']").on('click', function(event) {

	   // Make sure this.hash has a value before overriding default behavior
	  if (this.hash !== "") {

	    // Store hash
	    var hash = this.hash;

	    // Using jQuery's animate() method to add smooth page scroll
	    // The optional number (900) specifies the number of milliseconds it takes to scroll to the specified area
	    $('html, body').animate({
	      scrollTop: $(hash).offset().top
	    }, 900, function(){

	      // Add hash (#) to URL when done scrolling (default click behavior)
	      window.location.hash = hash;
	      });
	    } // End if
	  });
	})
	</script>
	<!-- /scroll to top -->
	<script src="js/ppbc/pricetable.js"></script>
	<script src="js/ppbc/wmBox.js"></script>
	<script src="js/ppbc/info.js"></script>
	<!-- js for pricing table pop up -->	
	<script src="js/ppbc/jquery.magnific-popup.js" type="text/javascript"></script>
		 <script>
			$(document).ready(function() {
			$('.popup-with-zoom-anim').magnificPopup({
				type: 'inline',
				fixedContentPos: false,
				fixedBgPos: true,
				overflowY: 'auto',
				closeBtnInside: true,
				preloader: false,
				midClick: true,
				removalDelay: 300,
				mainClass: 'my-mfp-zoom-in'
			});
																			
			});
		</script>
	<!-- /js for pricing table pop up -->
	<!-- js for back to top -->
	<script src="js/ppbc/top.js"></script>
	<!-- /js for back to top -->
	<!-- js for search button -->
	<script src="js/ppbc/classie.js"></script>
	<script src="js/ppbc/uisearch.js"></script>
	<script>
		new UISearch( document.getElementById( 'sb-search' ) );
	</script>
	<!-- /js for search button -->
	<script src="js/ppbc/jquery.pogo-slider.min.js"></script>
	<script src="js/ppbc/main.js"></script>
	<!-- /js files -->
</body>
</html>