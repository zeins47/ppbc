<section class="banner-inner">
	
</section>
<!-- /inner pages banner section -->
<!-- inner headers -->
<section class="inner-header">
    <div class="container">
        <!-- Page Heading/Breadcrumbs -->
        <div class="row">
            <div class="col-lg-12">
                <h2 class="page-header">Tuga Pokok & Fungsi</h2>
                <ol class="breadcrumb">
                    <li><a href="index.html">Home</a>
                    </li>
                    <li class="active">Tuga Pokok & Fungsi</li>
                </ol>
            </div>
        </div>
        <!-- /.row -->
	</div>
</section>
<!-- /inner headers -->
<!-- about section -->
<section class="about-w3ls">	
    <div class="container">    
		<!-- Intro Content -->
        <div class="row">
            <div class="col-md-6">
				<div class="hover01 column">
					<div>
						<figure><img class="img-responsive" src="images/about-img.jpg" alt="w3layouts" title="w3layouts"></figure>
					</div>
                </div>
            </div>
            <div class="col-md-6">
                <h3>About Our Business</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sed voluptate nihil eum consectetur similique? Consectetur, quod, incidunt, harum nisi dolores delectus reprehenderit voluptatem perferendis dicta dolorem non blanditiis ex fugiat.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Saepe, magni, aperiam vitae illum voluptatum aut sequi impedit non velit ab ea pariatur sint quidem corporis eveniet. Odit, temporibus reprehenderit dolorum!</p>
            </div>
        </div>
        <!-- /.row -->
	</div>
</section>
<!-- /about section -->	
<!-- team section -->
<section class="team" style="background-color:#eb5425;">
	<div class="container">
		<h3 class="text-center w3layouts w3 w3l w3ls agileits-w3layouts agile w3-agile" style="color:#fff;">Our Amazing Team</h3>
		<p class="text-center w3layouts w3 w3l w3ls agileits-w3layouts agile w3-agile" style="color:#fff;">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
		<div class="row">
			<div class="col-lg-3 col-md-3 col-sm-6 team-w3ls">
				<div class="grid">
					<figure class="effect-julia">
						<img src="images/team-img1.jpg" alt="w3layouts" class="img-responsive" title="w3layouts"/>
						<figcaption>
							<div>
								<ul class="social-icons2">	
									<li><a href="#"><i class="fa fa-facebook"></i></a></li>
									<li><a href="#"><i class="fa fa-youtube"></i></a></li>
									<li><a href="#"><i class="fa fa-twitter"></i></a></li>
									<li><a href="#"><i class="fa fa-whatsapp"></i></a></li>
									<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
								</ul>	
							</div>
						</figcaption>			
					</figure>
				</div>
				<h4 class="text-center" style="color:#fff;">Elizabeth</h4>
				<p class="team-p1" style="color:#fff;">Founder</p>	
			</div>
			<div class="col-lg-3 col-md-3 col-sm-6 team-w3ls">
				<div class="grid">
					<figure class="effect-julia">
						<img src="images/team-img2.jpg" alt="w3layouts" class="img-responsive" title="w3layouts"/>
						<figcaption>
							<div>
								<ul class="social-icons2">	
									<li><a href="#"><i class="fa fa-facebook"></i></a></li>
									<li><a href="#"><i class="fa fa-youtube"></i></a></li>
									<li><a href="#"><i class="fa fa-twitter"></i></a></li>
									<li><a href="#"><i class="fa fa-whatsapp"></i></a></li>
									<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
								</ul>	
							</div>
						</figcaption>			
					</figure>
				</div>
				<h4 class="text-center" style="color:#fff;">Diana</h4>
				<p class="team-p1" style="color:#fff;">President</p>	
			</div>
			<div class="col-lg-3 col-md-3 col-sm-6 team-w3ls">
				<div class="grid">
					<figure class="effect-julia">
						<img src="images/team-img3.jpg" alt="w3layouts" class="img-responsive" title="w3layouts"/>
						<figcaption>
							<div>
								<ul class="social-icons2">	
									<li><a href="#"><i class="fa fa-facebook"></i></a></li>
									<li><a href="#"><i class="fa fa-youtube"></i></a></li>
									<li><a href="#"><i class="fa fa-twitter"></i></a></li>
									<li><a href="#"><i class="fa fa-whatsapp"></i></a></li>
									<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
								</ul>	
							</div>
						</figcaption>			
					</figure>
				</div>
				<h4 class="text-center" style="color:#fff;">Max Payne</h4>
				<p class="team-p1" style="color:#fff;">Secretary</p>	
			</div>
			<div class="col-lg-3 col-md-3 col-sm-6 team-w3ls">
				<div class="grid">
					<figure class="effect-julia">
						<img src="images/team-img4.jpg" alt="w3layouts" class="img-responsive" title="w3layouts"/>
						<figcaption>
							<div>
								<ul class="social-icons2">	
									<li><a href="#"><i class="fa fa-facebook"></i></a></li>
									<li><a href="#"><i class="fa fa-youtube"></i></a></li>
									<li><a href="#"><i class="fa fa-twitter"></i></a></li>
									<li><a href="#"><i class="fa fa-whatsapp"></i></a></li>
									<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
								</ul>	
							</div>
						</figcaption>			
					</figure>
				</div>
				<h4 class="text-center" style="color:#fff;">Johnny Blaze</h4>
				<p class="team-p1" style="color:#fff;">Manager</p>				
			</div>
		</div>	
	</div>
</section>
<!-- /team section -->
<!-- customer section -->
<section class="customer">        
	<div class="container">
		<h3 class="text-center agileits agileinfo wthree w3-agileits">Our Customers</h3>
		<p class="text-center agileits agileinfo wthree w3-agileits">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
        <div class="row">
            <div class="col-md-2 col-sm-4 col-xs-6 cust-w3ls">
				<div class="hover01 column">
					<div>
						<figure><img class="img-responsive customer-img" src="images/cust-img1.jpg" alt="w3layouts" title="w3layouts"></figure>
					</div>
                </div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 cust-w3ls">
                <div class="hover01 column">
					<div>
						<figure><img class="img-responsive customer-img" src="images/cust-img2.jpg" alt="w3layouts" title="w3layouts"></figure>
					</div>
                </div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 cust-w3ls">
                <div class="hover01 column">
					<div>
						<figure><img class="img-responsive customer-img" src="images/cust-img3.jpg" alt="w3layouts" title="w3layouts"></figure>
					</div>
                </div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 cust-w3ls">
                <div class="hover01 column">
					<div>
						<figure><img class="img-responsive customer-img" src="images/cust-img4.jpg" alt="w3layouts" title="w3layouts"></figure>
					</div>
                </div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 cust-w3ls">
                <div class="hover01 column">
					<div>
						<figure><img class="img-responsive customer-img" src="images/cust-img5.jpg" alt="w3layouts" title="w3layouts"></figure>
					</div>
                </div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 cust-w3ls">
                <div class="hover01 column">
					<div>
						<figure><img class="img-responsive customer-img" src="images/cust-img6.jpg" alt="w3layouts" title="w3layouts"></figure>
					</div>
                </div>
            </div>
        </div>
        <!-- /.row -->
	</div>
</section>